"""Task1"""

"""numpy.fromfile or numpy.memmap

numpy.fromfile
has do load everything in a whole. Can not load just a bit of the data

numpy.memmap
Create a memory-map to an array stored in a binary file on disk.
Memory-mapped files are used for accessing small segments of large files on disk, without reading the entire file into memory. NumPy’s memmap’s are array-like objects. This differs from Python’s mmap module, which uses file-like objects.

"""

import numpy as np
import matplotlib.pyplot as plt

"""Read in datafile containing the earth structure"""
earth_map = np.fromfile("gl-latlong-1km-landcover.bsq", dtype= np.uint8, count=- 1)
"count=-1 means it reads the whole file"

print(earth_map)

"""
Storage: first index is line, second is position
Make matrix out of vektor.
"""
earth_map = np.reshape(earth_map, (21600,43200))
print(earth_map)


#Show whole map, but only every 50th point in x and y direction as then the plot is much much faster
plt.imshow(earth_map[::50,::50])
#plt.show()


def calculate_pixels(n, o):
	o_pic = int(43200/360*(o+180))-1
	n_pic = int(21600/180*(90-n))-1
	return  n_pic, o_pic
	

#print(earth_map[n_pic, o_pic])

def determine_landscape(n, o):
	n_pic, o_pic = calculate_pixels(90,-180)
	return earth_map[n_pic, o_pic]

